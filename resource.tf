

resource "random_string" "test" {
  length  = 8
  number  = true
  lower   = true
  upper   = false
  special = false
}

resource "azurerm_resource_group" "my-demo" {
  name     = var.name
  location = var.location
}

resource "azurerm_storage_account" "test-demo" {
  name                     = local.storage_account_name
  resource_group_name      = azurerm_resource_group.my-demo.name
  location                 = azurerm_resource_group.my-demo.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  enable_https_traffic_only = var.enable_https_traffic


   dynamic "static_website" {
    for_each = local.if_static_website_enabled
    content {
      index_document     = var.index_path
      error_404_document = var.custom_404_path
    }
  }

  identity {
    type = var.assign_identity ? "SystemAssigned" : null
  }


}

resource "null_resource" "copyfilesweb" {
  count = var.enable_static_website ? 1 : 0
  provisioner "local-exec" {
    command = "az storage blob upload-batch --no-progress --account-name ${azurerm_storage_account.test-demo.name} --account-key ${azurerm_storage_account.test-demo.primary_access_key}  -s ${var.static_website_source_folder} -d '$web' --output none"
  }
}




