
data "archive_file" "azure_function_package" {
  type        = "zip"
  source_dir  = local.temp_dir
  output_path = local.temp_package_zip_path
}

resource "azurerm_storage_container" "function_packages" {
  name                  = local.function_package_container_name
  storage_account_name  = azurerm_storage_account.test-demo.name
  container_access_type = "private"
}


resource "azurerm_storage_blob" "function" {
  name                   = "fn-${local.now_friendly}.zip"
  storage_account_name   = azurerm_storage_account.test-demo.name
  storage_container_name = azurerm_storage_container.function_packages.name
  type                   = "Block"
  metadata = {
    sha1   = data.archive_file.azure_function_package.output_sha
    sha256 = data.archive_file.azure_function_package.output_base64sha256
    md5    = data.archive_file.azure_function_package.output_md5
  }
  source = data.archive_file.azure_function_package.output_path
}


data "azurerm_storage_account_sas" "package" {
  connection_string = azurerm_storage_account.test-demo.primary_connection_string
  https_only        = true

  resource_types {
    service   = false
    container = false
    object    = true
  }

  services {
    blob  = true
    queue = false
    table = false
    file  = false
  }

  start  = local.now
  expiry = local.in_ten_years

  permissions {
    read    = true
    write   = false
    delete  = false
    list    = false
    add     = false
    create  = false
    update  = false
    process = false
  }
}


resource "azurerm_app_service_plan" "function-plan" {
  name                = "azure-functions-test-service-plan"
  location            = azurerm_resource_group.my-demo.location
  resource_group_name = azurerm_resource_group.my-demo.name
  kind                = "FunctionApp"
  reserved            = true

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_function_app" "function-app" {
  name                       = var.name
  location                   = azurerm_resource_group.my-demo.location
  resource_group_name        = azurerm_resource_group.my-demo.name
  app_service_plan_id        = azurerm_app_service_plan.function-plan.id
  storage_account_name       = azurerm_storage_account.test-demo.name
  storage_account_access_key = azurerm_storage_account.test-demo.primary_access_key
  os_type                    = "linux"
  site_config {
    cors {
      allowed_origins = ["https://${azurerm_storage_account.test-demo.primary_web_host}" ]
    }
  }
  app_settings = {
    "WEBSITE_RUN_FROM_PACKAGE"    = "https://${azurerm_storage_account.test-demo.name}.blob.core.windows.net/${azurerm_storage_container.function_packages.name}/${azurerm_storage_blob.function.name}${data.azurerm_storage_account_sas.package.sas}",
    FONCTIONS_WORKER_RUNTIME = "node",
    "AzureWebJobsDisableHomepage" = "true",
  }

   depends_on = [
     data.archive_file.azure_function_package
  ]
}


