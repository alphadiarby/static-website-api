locals {
  name_without_special_char = replace(var.name, "/[^\\w]*/", "")
  storage_account_name_slug = substr(lower(local.name_without_special_char), 0, 10)
  storage_account_name      = "sa${local.storage_account_name_slug}${random_string.test.result}"
  function_package_container_name = "${var.name}-static-site-az-fn-packages"

  if_static_website_enabled = var.enable_static_website ? [{}] : []

  now          = timestamp()
  in_ten_years = "${(tonumber(formatdate("YYYY", local.now)) + 10)}-${formatdate("MM-DD'T'hh:mm:ssZ", local.now)}"
  temp_dir                  = "${path.root}/api"
  temp_package_zip_path     = "${local.temp_dir}/api.zip"
  now_friendly = formatdate("YYYY-MM-DD-hh-mm-ss", local.now)
 
}