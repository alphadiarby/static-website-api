variable "name" {
  type        = string
  default = "my-resource-rg"
}

variable "location" {
  type        = string
  description = "Azure region in which resources will be located"
  default     = "West Europe"
}

variable "static_content_directory" {
  type        = string
  default = "/home/alphadi/website/website"
}


variable "enable_https_traffic" {
  description = "Configure the storage account to accept requests from secure connections only. Possible values are `true` or `false`"
  default     = true
}
variable "enable_static_website" {
  description = "Controls if static website to be enabled on the storage account. Possible values are `true` or `false`"
  default     = true
}

variable "static_website_source_folder" {
  description = "Set a source folder path to copy static website files to static website storage blob"
  default     = "/home/alphadi/website/website"
}

variable "assign_identity" {
  description = "Specifies the identity type of the Storage Account. At this time the only allowed value is SystemAssigned."
  default     = true
}

variable "index_path" {
  description = "path from your repo root to index.html"
  default     = "index.html"
}

variable "custom_404_path" {
  description = "path from your repo root to your custom 404 page"
  default     = "404.html"
}


variable "custom_mime_mappings" {
  type        = map(string)
  description = "Add or replace content-type mappings by setting this value. Ex: `{ \"text\" = \"text/plain\", \"new\" = \"text/derp\" }`"
  default     = null
}