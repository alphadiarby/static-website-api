output "storage_account_id" {
  value       = azurerm_storage_account.test-demo.id
  description = "The ID of the storage account."
}

output "storage_account_name" {
  value       = azurerm_storage_account.test-demo.name
  description = "The name of the storage account."
}

output "storage_primary_connection_string" {
  value       = azurerm_storage_account.test-demo.primary_connection_string
  sensitive   = true
  description = "The primary connection string for the storage account."
}

output "storage_primary_access_key" {
  value       = azurerm_storage_account.test-demo.primary_access_key
  sensitive   = true
  description = "The primary access key for the storage account."
}


output "static_website_url" {
  value       = "https://${azurerm_storage_account.test-demo.primary_web_host}"
  description = "static web site URL from storage account"
}

# output "static_website_cdn_endpoint_hostname" {
#   value       = element(concat(azurerm_cdn_endpoint.cdn-test.*.name, [""]), 0)
#   description = "CDN endpoint URL for Static website"
# }

output "azure_function_defualt_url" {
  description = "The Azure Functions application's default URL"
  value       = "https://${azurerm_function_app.function-app.default_hostname}"
}


output "test" {
  value = azurerm_storage_account.test-demo.secondary_blob_endpoint
}